package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

import java.text.NumberFormat;

/**
 * A guessing game!
 */
public class GuessingGame {
    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */

    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {
            try {
                guess = getUserGuess();


                if (guess > 100 || guess < 1) {
                    System.out.println("Value is out of a valid range (1-100 inclusive)");
                } else if (guess > number) {
                    System.out.println("Too high!");
                } else if (guess < number) {
                    System.out.println("Too low!");
                } else {
                    System.out.println("Perfect!");
                }
            } catch (NumberFormatException e) {
                System.out.println("Value must be an integer whole number");
            }
        }
    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */

    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */

    private int getUserGuess() {
        System.out.print("Enter your guess: ");
        return Integer.parseInt(Keyboard.readInput());
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}




































































    /*

    private void tryCatch01(){
        int result =0;
        int[]nums =null;
        try {result=nums.length;
            System.out.println("See you");
        }catch (ArithmeticException e){
            result =-1;
        }
        System.out.println("R" + result);
    }

    private void tryCatch02(){
        int num1 = 120, num2 = 120, result =0;
        try {
            result = num2/(num1-num2);
        }catch (ArithmeticException e){
            result =-1;
        }
        System.out.println("Result" + result);


    }

    private void tryCatch03() {
        int result = 0;
        String[] items = {"one", "two", null};
        try {
            result = items[2].length();
        } catch (NullPointerException e) {

        }
        System.out.println("R" + result);
    }


    private void tryCatch04(){
        int num = 0;
        try {
            System.out.println("Enter no.");
            num = Integer.parseInt(Keyboard.readInput());
            System.out.println("Thanks");
        }catch (NumberFormatException e){
            System.out.println("Input error");
            num = -1;
        }
        System.out.println("No. " + num);

    }

    private int tryCatch05(){
        int result = 0;
        int[] nums = {2, 3 , 4, -1, 4};

        try {
            result = nums[nums[3]];
            System.out.println("See you");
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Number error: ");
            result = -1;
        }
        return result;



    }

    private void tryCatch06(){
        try {
            try06(0,"");
            System.out.println("A");

        }catch (ArithmeticException e){
            System.out.println("B error");
        }
    }

    private void try06(int num, String s){
        System.out.println("C");
        try {
            num = s.length();
            num = 200/num;

        }catch (NullPointerException e){
            System.out.println("E error");
        }
        System.out.println("F");



    }

    private void tryCatch07(){

            try07(0, null);
            System.out.println("A");

        }

    private void try07(int num, String s){
        System.out.println("B");
        try {
            num = s.length();

        }catch (NullPointerException e){
            System.out.println("Null pointer error");

        }
        System.out.println("C");


    }

    private void tryCatch08(){
        try{
            try08(0, null);
            System.out.println("A");
        }catch (NullPointerException e){
            System.out.println("B");
        }
    }

    private void try08(int num, String s){
        System.out.println("C");
        try {
            num = s.length();
            System.out.println("D");
        }finally {
            System.out.println("E");
        }
        System.out.println("F");
    }



    private void throwsClause09(){
        try {
            throws09(null);
            System.out.println("A");
        }catch (NullPointerException e){
            System.out.println(e);
        }
        System.out.println("B");

    }


    private void throws09(String numS) throws NullPointerException{
        if (numS == null){
            throw new NullPointerException("Null String");
        }
        System.out.println("C");


    }
    private void throwsClause10(){
        try {
            throws10(null);
            System.out.println("A");
        }catch (ArithmeticException e){
            System.out.println(e);
        }finally {
            System.out.println("B");
        }
        System.out.println("C");
    }

    private void throws10(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }



*/





